### BTS SIO 
### SISR 2e année avec Mme ABDELMOULA
Année scolaire 2022-2023


### Commandes Systèmes à maîtriser (faire une fiche personelle de leur utilisation)

- `cd` et ses options
- `mkdir`et ses options
- `cp`, `rm`et `mv`et leurs options
- `pwd`et ses options
- `touch` et ses options
- `ls` et ses options
- `history` et ses options
- `cat`, `more`, `most` et `less` et leurs options
- `grep` et ses options
- `man` et ses options

### Outils de travail en ligne de commande

- `pandoc` et ses options
- `ssh`et ses options
- `scp`, `rsync` et leurs options
- `ip` et ses options
- `ttyrec` et `ttyplay` et leurs options
- `tshark` et ses options 
- `nano` et ses options
- `git` ou `tig` et ses options
- `etckeeper` et ses options

