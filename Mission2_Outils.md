% Mission Outils : Etckeeper, tig et Tshark
% [Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), Lycée de l'Hautil
% Cours SISR2 (BTS - SIO) - 04/10/2022





## Objectif

Mettre en place un gestionnaire de version pour la partie système `etckeeper`, la visualiser avec `tig`.
Mettre en place une capture de trames avec `Tshark` pour constater le bon fonctionnement du serveur SSH.

Quelques liens pour vous accompagner : 
- <https://www.digitalocean.com/community/tutorials/how-to-manage-etc-with-version-control-using-etckeeper-on-centos-7>
- <https://ubuntu.com/server/docs/tools-etckeeper>
- <https://blog.wescale.fr/tig-la-console-pour-git>
- <https://www.kali-linux.fr/configuration/tshark-analyse-paquet-sniffing>


Un **compte rendu** détaillé de votre activité devra être déposé sur GitLab  dans SISR2 (formats Markdown et/ou PDF bien identifiés) : `Mission2_Outils.md` `Mission2_Outils.pdf`



## Etape 0 - Préparer la machine de test

- Cloner une machine Debian11 légère (sans bureau graphique),
- Vérifier le fichier `sources.list` 
- la mettre à jour.


## Etape 1 - Installer etckeeper

Installer etckeeper sur la **nouvelle** machine clônée pour suivre les changements de configuration
- Que permet de faire précisement ce nouveau paquet ?
- Quelles commandes sont importantes à connaître ?

A faire :
- Intitailiser le dépôt des configurations 
- Montrer une vue des MAJ enregistrées par etckeeper

## Etape 2 - Installer tig

Installer tig sur la **même** machine dédiée au serveur SSH.
- Que permet de faire précisement ce nouveau paquet ?
- Quelles commandes sont importantes à connaître ?

A faire :
- Montrer une vue des MAJ enregistrées par etckeeper
- Valider la nouvelle MAJ si nécessaire 
- Montrer une vue des MAJ avec tig

## Etape 3 - Installer le serveur SSH avec tasksel

- Installer avec l'utilitaire debian `tasksel` un serveur SSH.

## Etape 4 - Installer Tshark

Installer Tshark sur toutes les machines du réseau-test
	(une application semblable à Wireshark en ligne de commande) 
- En quoi cette installation sera utile ?
- Quelle est la commande utilisée pour l'installation ?
- Quelles commandes permettent de : 
	- voir le trafic réseau en direct
	- d'enregistrer le trafic réseau dans un fichier pcap
	- de filtrer et voir le trafic réseau en direct
	- de filtrer et enregistrer le trafic réseau dans un fichier pcap
	
## Etape 5 - Capture des trames SSH 

Démarrer une capture sur la machine accueillant le serveur SSH 

* choisir un filtre qui permette de voir seulement les paquets SSH
* initier des demande de nouvelles adresses IP ou de renouvellement de bail par les machines du réseau-test 
* Enregistrer un fichier pcap 
* Utiliser le protocole SSH pour récupérer sur votre machine hôte votre fichier pcap à l'aide la commande `rsync`. Noter la lignes de commande utilisée en expliquant ses différents éléments
* Ouvrir le fichier pcap avec votre interface graphique Wireshark, installée sur votre machine hôte. Repérer les différentes étapes de conversation entre les machines du réseau-test et votre serveur SSH




