### BTS SIO 
## SISR 2e année avec Mme ABDELMOULA
Année scolaire 2022-2023


### Missions à faire et prendre en main (ou pas !) 


- **Mission0_VM** : installation d'une machine virtuelle avec  Debian11 sans bureau
- **Mission1_SSH** : installation et utilisation d'un client et serveur SSH avec des machines virtuelles Debian11 sans bureau
- **Mission2_Outils** : installation et utilisation de plusieurs outils (etckeeper, tig, tshark) dans une machine virtuelle Debian11 sans bureau


### Etude de cas travaillé (ou pas !) 

- **Etude de cas CUB** : [Sujet](SujetBTS_cub.pdf) 
	- Dossier A : à finir avant vendredi 14/10/2022
	- Dossier B : à finir avant vendredi 22/10/2022


### Commandes/outils vues ou à voir (ou pas !) : [src](CommandesSys.md)